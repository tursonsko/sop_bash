#!/bin/bash

# zad2-2
FILE=/home/wojtek/SOP_BASH/bash03042020/zad2_31.sh

if [[ -f $FILE ]]; then
    echo `./zad2_1.sh a b c d e`
elif [ $# -gt 0 ]; then 
    echo `./$1 ${@:2}`        #niech wykona plik podany jako parametr
else
    echo "error"            #komunikat o bledzie
fi
